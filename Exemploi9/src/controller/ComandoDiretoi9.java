package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import com.fazecast.jSerialComm.SerialPort;

public class ComandoDiretoi9 {
	
	SerialPort portaCom;
	
	public SerialPort abrindoPorta(String porta) {
		
		portaCom = SerialPort.getCommPort(porta);
		portaCom.setBaudRate(115200);
		portaCom.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
		portaCom.setNumDataBits(5);
		portaCom.setNumStopBits(1);
		portaCom.setParity(SerialPort.NO_PARITY);
		portaCom.openPort();
		
		if(portaCom.isOpen()) {
			System.out.println("Porta Aberta com Sucesso.");
		} else {
			System.out.println("Falha na abertura de porta.");
		}
		return portaCom;
	}
	public int menuPrincipal() {
		
		System.out.println("**************************************************\n");
        System.out.println("*                                                *\n");
        System.out.println("*           EXEMPLO I9 - COMANDO DIRETO          *\n");
        System.out.println("*                                                *\n");
        System.out.println("**************************************************\n\n\n");
        System.out.println("�   [1]    IMPRESS�O CUPOM PADR�O - ELGIN.\n");
        System.out.println("�   [2]    ABRIR GAVETA.\n");
        System.out.println("�   [3]    STATUS DE IMPRESSORA, PAPEL E GAVETA.\n");
        System.out.println("�   [4]    IMPRESS�O CUPOM LAYOUT NOVO - ELGIN.\n");
        System.out.println("�   [5]    IMPRESS�O DE TODOS OS CODE128\n");
        System.out.println("�   [6]    IMPRESS�O DE TODAS AS FONTES E TAMANHOS.\n");
        System.out.println("�   [7]    FINALIZAR.\n");
        
        Scanner teclado = new Scanner(System.in);
        int t = teclado.nextInt();
        
        switch(t) {
        case 1:
        	CupomPadrao();
        	break;
        case 2:
        	AbrirGaveta();
        	break;
        case 3:
        	Status();
        	break;
        case 4:
        	CupomNovo();
        	break;
        case 5:
        	CodigosDeBarra();
        	break;
        case 6:
        	Fontes();
        	break;
        case 7:
        	portaCom.closePort();
        	System.out.println("Porta Fechada com Sucesso.");
        	break;
        }
        
        return t;
        
	}
	public void CupomPadrao() {
		
		// DATA E HORA ATUAL
        Date agora = new Date();
        
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String data2 = format.format(agora);
		
		 // LIMPANDO O BUFFER DE IMPRESS�O.
        byte[] comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("Elgin Manaus\n".getBytes(),"Elgin Manaus\n".length());
        portaCom.writeBytes("Elgin/SA\n".getBytes(),"Elgin/SA\n".length());
        portaCom.writeBytes("Rua Abiurana, 579 Distrito Industrial Manaus - AM\n\n".getBytes(),
                "Rua Abiurana, 579 Distrito Industrial Manaus - AM\n\n".length());
        portaCom.writeBytes("CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM\n".getBytes(),
                 "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        // CANCELANDO FONTE ANTERIOR
        comando = new byte[]{0x1B,0x21,0x0};
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MUDANDO FONTE.
        comando = new byte[]{0x1B,0x21,0x08};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESSOS.
        portaCom.writeBytes("EXTRATO No. 002046\n".getBytes(),"EXTRATO No. 002046\n".length());
        portaCom.writeBytes("CUPOM FISCAL ELETRONICO - SAT\n".getBytes(),"CUPOM FISCAL ELETRONICO - SAT\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("CPF/CNPJ consumidor: \n".getBytes(),"CPF/CNPJ consumidor: \n".length());
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x0};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        portaCom.writeBytes("# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM R$\n".getBytes(),
                "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM R$\n".length());
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        portaCom.writeBytes("1 0000000 IMPRESSORA N�O FISCAL I9/I7 kg 1 KG X 1.290     10,00\n\n".getBytes(),
                "1 0000000 IMPRESSORA N�O FISCAL I9/I7 kg 1 KG X 1.290     10,00\n\n".length());
        
     // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x0};
        portaCom.writeBytes(comando,comando.length);

        
        portaCom.writeBytes("TOTAL R$                                                  10,00\n".getBytes(),
                "TOTAL R$                                                  10,00\n".length());
        
        
        
        portaCom.writeBytes("Dinheiro                                                  10,00\n".getBytes(),
                "Dinheiro                                                  10,00\n".length());
        portaCom.writeBytes("Troco R$                                                   0,00\n\n".getBytes(),
                "Troco R$                                                   0,00\n\n".length());
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        portaCom.writeBytes("Tributos Totais (Lei Fed 12.741/12) R$                      3,85\n".getBytes(),
                "Tributos Totais (Lei Fed 12.741/12) R$                      3,85\n".length());
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est\n".getBytes(),
                "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est\n".length());
        portaCom.writeBytes("Fonte: IBPT/FECOMECIO (RS) 9oi3aC\n\n".getBytes(),
                "Fonte: IBPT/FECOMECIO (RS) 9oi3aC\n\n".length());
        portaCom.writeBytes("KM: 0\n".getBytes(),
                "KM: 0\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("* Valor aproximado dos tributos do item\n".getBytes(),
                "* Valor aproximado dos tributos do item\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes("----------------------------------------------------------------\n".getBytes(),
                "----------------------------------------------------------------\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CANCELANDO FONTE ANTERIOR0
        comando = new byte[]{0x1B,0x21,0x0};
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MUDANDO FONTE.
        comando = new byte[]{0x1B,0x21,0x08};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESSOS.
        portaCom.writeBytes("SAT No. 900001231\n".getBytes(),"SAT No. 900001231\n".length());
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // MANDAR IMPRESSOS.
        portaCom.writeBytes(data2.getBytes(),data2.length());
        comando = new byte[]{0xA,0xA};
        portaCom.writeBytes(comando,comando.length);
        
        
        /********************************************************** 
         **********************************************************
         *********IMPRESS�O DO CODE 128 - C�DIGO DE BARRAS*********
         **********************************************************
         **********************************************************/
        
        
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO ALTURA DO C�DIGO DE BARRAS.
        comando = new byte[]{0x1D,0x68,70};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO LARGURA DO C�DIGO DE BARRAS.
        comando = new byte[]{0x1D,0x77,1};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO POSI��O DOS CARACTERES EM HRI.
        comando = new byte[]{0x1D,0x48,1};
        portaCom.writeBytes(comando,comando.length);
        
        String code128 = "{B35150661099008000141593515066109900800014159";
        int tamanho = code128.length();
        
        // MANDAR IMPRESSOS.
        comando = new byte[]{0x1D,0x6B,0x49,(byte)tamanho};
        portaCom.writeBytes(comando,comando.length);
        portaCom.writeBytes(code128.getBytes(),code128.length());
        comando = new byte[]{0xA,0xA};
        portaCom.writeBytes(comando,comando.length);
        
        
        /**********************************************************\ 
         **********************************************************
         *****************IMPRESS�O DO QR CODE*********************
         **********************************************************
        \**********************************************************/
        
        
        int pl;
        int ph;
        String qrcode = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmoxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrubensborgesdeandrade";
        int taman = qrcode.length()+3;
        
        if(taman>=255) {
            pl = taman%256;
            ph = taman/256;
            
        } else {
            
            pl = taman;
            ph = 0;
        }
        
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{27, 97, 49};
        portaCom.writeBytes(comando,comando.length);
        // DEFININDO O TAMANHO DO QRCODE
        comando = new byte[]{29, 40, 107, 3, 0, 49, 67, 5};
        portaCom.writeBytes(comando,comando.length);
        // DEFINIR N�VEL DE CORRE��O
        comando = new byte[]{29, 40, 107, 3, 0, 49, 69, 48};
        portaCom.writeBytes(comando,comando.length);
        // ARMAZENAMENTO DE DADOS.
        comando = new byte[]{0x1D, 0x28, 0x6B, (byte)pl, (byte)ph, 0x31, 0x50, 0x30};
        portaCom.writeBytes(comando,comando.length);
        comando = qrcode.getBytes();
        portaCom.writeBytes(comando,comando.length);
        comando = new byte[]{29, 40, 107, 3, 0, 49, 81, 48};
        portaCom.writeBytes(comando,comando.length);
        // PULAR LINHA
        comando = new byte[]{0xA};
        portaCom.writeBytes(comando,comando.length);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        portaCom.writeBytes(comando,comando.length);
	}
	public void AbrirGaveta() {
		
		// ABRIR GAVETA
        byte[] comando = new byte[]{27, 112, 0, 100, 100}; 
        portaCom.writeBytes(comando,comando.length);
		
	}
	public void Status() {
		
		// COMANDO PARA STATUS
        byte[]comando = new byte[]{0x1D, 0x61, 0x04}; 
        portaCom.writeBytes(comando,comando.length);
        // ARMAZENANDO RETORNO.
        byte[]commando = new byte[4];
        portaCom.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 0, 0);
        portaCom.readBytes(commando, commando.length);
            switch (commando[0]) {
                case 20:
                    System.out.println("STATUS DA IMPRESSORA:\n");
                    System.out.println("Tampa e Gaveta fechada.\n\n");
                    break;
                case 16:
                    System.out.println("STATUS DA IMPRESSORA:\n");
                    System.out.println("Gaveta aberta.\n\n");
                    break;
                case 60:
                    System.out.println("STATUS DA IMPRESSORA:\n");
                    System.out.println("Tampa aberta.\n\n");
                    break;
                case 56:
                    System.out.println("STATUS DA IMPRESSORA:\n");
                    System.out.println("Tampa e Gaveta aberta.\n\n");
                    break;
                default:
                    System.out.println("STATUS DA IMPRESSORA:\n");
                    System.out.println("Erro no retorno de status.\n\n");
                    
            }
            
            switch (commando[2]) {
                case 0:
                    System.out.println("STATUS DE PAPEL:\n");
                    System.out.println("Bobina em n�vel ideal.\n\n");
                    break;
                case 3:
                    System.out.println("STATUS DE PAPEL:\n");
                    System.out.println("Aten��o! Bobina com n�vel baixo.\nFavor verificar.\n\n");
                    break;
                case 15:
                    System.out.println("STATUS DE PAPEL:\n");
                    System.out.println("Aten��o! Sua bobina acabou.\nFavor verificar.\n\n");
                    break;
                default:
                    System.out.println("STATUS DE PAPEL:\n");
                    System.out.println("Erro no retorno de status.\n\n");
            }
	}
	public void CupomNovo() {
		
		// LIMPANDO O BUFFER
        byte[]comando = new byte[]{0x18};
        portaCom.writeBytes(comando, comando.length);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, 24, 00};
        portaCom.writeBytes(comando, comando.length);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, 0x10, 0x00};
        portaCom.writeBytes(comando, comando.length);
		// IMAGEM
		comando = new byte[]{0x1C, 0x70, 0x01, 0x00}; 
        portaCom.writeBytes(comando,comando.length);
		// SELE��O DO MODO P�GINA.
        comando = new byte[]{0x1B, 0x4C}; 
        portaCom.writeBytes(comando,comando.length);
        
        
        // DEFININDO �REA DE IMPRESS�O.
        comando = new byte[]{0x1B, 0x57, 0x00, 0x00, 0x00, 0x00, 0x48, 0x02, 24, 06}; 
        portaCom.writeBytes(comando,comando.length);
        
        // SELE��O DA DIRE��O DE IMPRESS�O NO MODO P�GINA.
        comando = new byte[]{0x1B, 0x54, 0x00};
        portaCom.writeBytes(comando, comando.length);
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        portaCom.writeBytes(comando, comando.length);
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, 0x10, 0x00};
        portaCom.writeBytes(comando, comando.length);
        
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        portaCom.writeBytes(comando,comando.length);
        
        portaCom.writeBytes("CNPJ 00.000.000/000-99 Razao Social da Empresa\n".getBytes(),
                "CNPJ 00.000.000/000-99 Razao Social da Empresa\n".length());
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Av da Tecnologia, 030, Centro, Rio de Janeiro, RJ\n".getBytes(),
                "Av da Tecnologia, 030, Centro, Rio de Janeiro, RJ\n".length());
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Doc. Aux. da Nota Fiscal de Consumidor Eletronica\n".getBytes(),
                "Doc. Aux. da Nota Fiscal de Consumidor Eletronica\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Codigo    Descricao        Qtde UN      Vl Unit    Vl Total\n".getBytes(),
                "Codigo    Descricao        Qtde UN      Vl Unit    Vl Total\n".length());
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("003277    Impressora Elgin I9   1 Peca   100.00      100.00\n".getBytes(),
                "003277    Impressora Elgin I9   1 Peca   100.00      100.00\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("085273    Impressora Elgin I7   1 Peca   100.00      100.00\n".getBytes(),
                "085273    Impressora Elgin I7   1 Peca   100.00      100.00\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Qtde. total de itens                                      2\n".getBytes(),
                "Qtde. total de itens                                      2\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Valor Total R$                                       200,00\n".getBytes(),
                "Valor Total R$                                       200,00\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Desconto R$                                           50,00\n".getBytes(),
                "Desconto R$                                           50,00\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Valor a Pagar R$                                     150,00\n\n".getBytes(),
                "Valor a Pagar R$                                     150,00\n\n".length());
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("FORMA PAGAMENTO                               VALOR PAGO R$\n".getBytes(),
                "FORMA PAGAMENTO                               VALOR PAGO R$\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Cartao de Credito                                75,00\n".getBytes(),
                "Cartao de Credito                                75,00\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Cartao de Credito                                75,00\n\n".getBytes(),
                "Cartao de Credito                                75,00\n\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x00};
        portaCom.writeBytes(comando,comando.length);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Consulte pela chave de acesso em\n".getBytes(),
                "Consulte pela chave de acesso em\n".length());
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x01};
        portaCom.writeBytes(comando,comando.length);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)0x95, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("http://nfce.fazenda.rj.gov.br/consulta \n".getBytes(),
                "http://nfce.fazenda.rj.gov.br/consulta \n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x40, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000\n".getBytes(),
                "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000\n".length());
        
        // QR CODE
        
        int pl2;
        int ph2;
        String qrcode2 = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmoxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrubensborgesdeandrade";
        int taman2 = qrcode2.length()+3;
        
        if(taman2>=255) {
            pl2 = taman2%256;
            ph2 = taman2/256;
            
        } else {
            
            pl2 = taman2;
            ph2 = 0;
        }
        
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // DEFININDO O TAMANHO DO QRCODE
        comando = new byte[]{29, 40, 107, 3, 0, 49, 67, 2};
        portaCom.writeBytes(comando,comando.length);
        // DEFINIR N�VEL DE CORRE��O
        comando = new byte[]{29, 40, 107, 3, 0, 49, 69, 48};
        portaCom.writeBytes(comando,comando.length);
        // ARMAZENAMENTO DE DADOS.
        comando = new byte[]{0x1D, 0x28, 0x6B, (byte)pl2, (byte)ph2, 0x31, 0x50, 0x30};
        portaCom.writeBytes(comando,comando.length);
        comando = qrcode2.getBytes();
        portaCom.writeBytes(comando,comando.length);
        comando = new byte[]{29, 40, 107, 3, 0, 49, 81, 48};
        portaCom.writeBytes(comando,comando.length);
        
        // IMPRESS�O DOS DEMAIS DADOS.
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x120, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x00};
        portaCom.writeBytes(comando,comando.length);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, (byte)0x186, 0x04};
        portaCom.writeBytes(comando, comando.length);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("CONSUMIDOR NAO IDENTIFICADO\n".getBytes(),
                "CONSUMIDOR NAO IDENTIFICADO\n".length());
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x01};
        portaCom.writeBytes(comando,comando.length);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        portaCom.writeBytes(comando, comando.length);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("NFC-E N0000001 Serie 001 04/12/2017 11:40:51".getBytes(),
                "NFC-E N0000001 Serie 001 04/12/2017 11:40:51".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Protocolo de autoriza��o: 314 1300004001 80\n".getBytes(),
                "Protocolo de autoriza��o: 314 1300004001 80\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Data de Autorizacao 04/12/2017 11:40:51\n".getBytes(),
                "Data de Autorizacao 04/12/2017 11:40:51\n".length());
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Trib Totais Incidentes(Lei Fed. 12.741/2012)\n".getBytes(),
                "Trib Totais Incidentes(Lei Fed. 12.741/2012)\n".length());
        // IMPRESS�O EM MODO P�GINA
        comando = new byte[]{0x0C};
        portaCom.writeBytes(comando, comando.length);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        portaCom.writeBytes(comando,comando.length);
		
	}
	public void CodigosDeBarra() {
		
		// CODE128
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       byte[]comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
        portaCom.writeBytes("Code128\n".getBytes(),
               "Code128\n".length());
       
       String acode128 = "{B1234567891011121";
       int atamanho = acode128.length();
       
       comando = new byte[]{0x1D,0x6B,0x49,(byte)atamanho};
       portaCom.writeBytes(comando,comando.length);
       portaCom.writeBytes(acode128.getBytes(),acode128.length());
       comando = new byte[]{0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE UPC-A
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code UPCA\n".getBytes(),
               "Code UPCA\n".length());
       
       String upcacode = "01234567012";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 0};
       portaCom.writeBytes(comando,comando.length);
       comando = upcacode.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE UPCE
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code UPCE\n".getBytes(),
               "Code UPCE\n".length());
       
       String upcecode = "01234567890";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 1};
       portaCom.writeBytes(comando,comando.length);
       comando = upcecode.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE EAN8
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code EAN8\n".getBytes(),
               "Code EAN8\n".length());
       
       String ean8code = "01234567";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 3};
       portaCom.writeBytes(comando,comando.length);
       comando = ean8code.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE EAN13
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code EAN13\n".getBytes(),
               "Code EAN13\n".length());
       
       String ean13code = "0123456789100";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 2};
       portaCom.writeBytes(comando,comando.length);
       comando = ean13code.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE39
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code39\n".getBytes(),
               "Code39\n".length());
       
       String code39 = "*0123456789*";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 4};
       portaCom.writeBytes(comando,comando.length);
       comando = code39.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE ITF
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("CodeITF\n".getBytes(),
               "CodeITF\n".length());
       
       String codeitf = "0123456789";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 5};
       portaCom.writeBytes(comando,comando.length);
       comando = codeitf.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE CODABAR
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Codabar\n".getBytes(),
               "Codabar\n".length());
       
       String codabar = "A40156B";
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 6};
       portaCom.writeBytes(comando,comando.length);
       comando = codabar.getBytes();
       portaCom.writeBytes(comando, comando.length);
       comando = new byte[]{0,0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       
       // CODE CODE93
//       TESTE DE COMMIT
       
       // LIMPANDO O BUFFER DE IMPRESS�O.
       comando = new byte[]{0x1B,0x40}; 
       portaCom.writeBytes(comando,comando.length);
       // CENTRALIZANDO O TEXTO.
       comando = new byte[]{0x1B,0x61,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO TIPO E TAMANHO DA FONTE.
       comando = new byte[]{0x1B,0x21,0x1};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x68,70};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
       comando = new byte[]{0x1D,0x77,2};
       portaCom.writeBytes(comando,comando.length);
       // DEFININDO POSI��O DOS CARACTERES EM HRI.
       comando = new byte[]{0x1D,0x48,2};
       portaCom.writeBytes(comando,comando.length);
       
       portaCom.writeBytes("Code93\n".getBytes(),
               "Code93\n".length());
       
       String code93 = "*TESTECODE93*";
       int code93tamanho = code93.length();
       
       // MANDAR IMPRESSOS.
       comando = new byte[]{29, 107, 72,(byte)code93tamanho};
       portaCom.writeBytes(comando,comando.length);
       portaCom.writeBytes(code93.getBytes(), code93.length());
       comando = new byte[]{0xA,0xA};
       portaCom.writeBytes(comando,comando.length);
       // CORTAR PAPEL
       comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
       portaCom.writeBytes(comando,comando.length);
		
	}
	public void Fontes() {
		
		// LIMPANDO O BUFFER DE IMPRESS�O.
        byte[]comando = new byte[]{0x1B,0x40}; 
        portaCom.writeBytes(comando,comando.length);
        // ALTURA
        comando = new byte[]{29,33,0};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 1\n".getBytes(), "Altura 1\n".length());
        // ALTURA
        comando = new byte[]{29,33,1};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 2\n".getBytes(), "Altura 2\n".length());
        // ALTURA
        comando = new byte[]{29,33,2};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 3\n".getBytes(), "Altura 3\n".length());
        // ALTURA
        comando = new byte[]{29,33,3};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 4\n".getBytes(), "Altura 4\n".length());
        // ALTURA
        comando = new byte[]{29,33,4};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 5\n".getBytes(), "Altura 5\n".length());
        // ALTURA
        comando = new byte[]{29,33,5};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 6\n".getBytes(), "Altura 6\n".length());
        // ALTURA
        comando = new byte[]{29,33,6};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 7\n".getBytes(), "Altura 7\n".length());
        // ALTURA
        comando = new byte[]{29,33,7};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Altura 8\n\n".getBytes(), "Altura 8\n\n".length());
        // LARGURA
        comando = new byte[]{29,33,0};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 1\n".getBytes(), "Largura 1\n".length());
        // LARGURA
        comando = new byte[]{29,33,16};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 2\n".getBytes(), "Largura 2\n".length());
        // LARGURA
        comando = new byte[]{29,33,32};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 3\n".getBytes(), "Largura 3\n".length());
        // LARGURA
        comando = new byte[]{29,33,48};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 4\n".getBytes(), "Largura 4\n".length());
        // LARGURA
        comando = new byte[]{29,33,64};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 5\n".getBytes(), "Largura 5\n".length());
        // LARGURA
        comando = new byte[]{29,33,80};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 6\n".getBytes(), "Largura 6\n".length());
        // LARGURA
        comando = new byte[]{29,33,96};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 7\n".getBytes(), "Largura 7\n".length());
        // LARGURA
        comando = new byte[]{29,33,112};
        portaCom.writeBytes(comando,comando.length);
        // MANDANDO IMPRESS�O
        portaCom.writeBytes("Largura 8\n\n".getBytes(), "Largura 8\n\n".length());
        // LARGURA/ALTURA
        comando = new byte[]{29,33,0};
        portaCom.writeBytes(comando,comando.length);
        // NEGRITO
        comando = new byte[]{27,69,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Com Negrito\n".getBytes(), "Com Negrito\n".length());// NEGRITO
        // SEM NEGRITO
        comando = new byte[]{27,69,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Sem Negrito\n\n".getBytes(), "Sem Negrito\n\n".length());
        
        // MODO DE IMPRESS�O FONTE B
        comando = new byte[]{27,77,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("MiniFont\n".getBytes(), "MiniFont\n".length());
        
        // MODO DE IMPRESS�O FONTE A
        comando = new byte[]{27,77,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Fonte Normal\n".getBytes(), "Fonte Normal\n".length());
        
        // MODO DE IMPRESS�O FONTE C
        comando = new byte[]{27,77,2};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Fonte C\n\n".getBytes(), "Fonte C\n\n".length());
        
        // MODO SUBLINHADO
        comando = new byte[]{27,33,(byte)128};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Sublinhado\n".getBytes(), "Sublinhado\n".length());
        // SEM SUBLINHADO
        comando = new byte[]{27,33,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Sem Sublinhado\n\n".getBytes(), "Sem Sublinhado\n\n".length());
        // ALINHADO A ESQUERDA
        comando = new byte[]{27,97,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Alinhado a Esquerda\n".getBytes(), "Alinhado a Esquerda\n".length());
        // ALINHAMENTO CENTRAL
        comando = new byte[]{27,97,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Centralizado\n".getBytes(), "Centralizado\n".length());
        // ALINHADO A DIREITA
        comando = new byte[]{27,97,2};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Alinhado a Direita\n\n".getBytes(), "Alinhado a Direita\n\n".length());
        // ALINHADO A ESQUERDA
        comando = new byte[]{27,97,0};
        portaCom.writeBytes(comando, comando.length);
        

        // CARACTERES ESPECIAIS            
        comando = new byte[]{0x1B, 0x40};
        portaCom.writeBytes(comando, comando.length);
        // TABELA DE C�DIGOS DE CARACTERES
        comando = new byte[]{27,116,16};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n".getBytes(), "������������������\n".length());
        // MODO SUBLINHADO
        comando = new byte[]{27,33,(byte)128};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n".getBytes(), "������������������\n".length());
        // SEM SUBLINHADO
        comando = new byte[]{27,33,0};
        portaCom.writeBytes(comando, comando.length);
        // NEGRITO
        comando = new byte[]{27,69,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n".getBytes(), "������������������\n".length());
    	// SEM NEGRITO
        comando = new byte[]{27,69,0};
        portaCom.writeBytes(comando, comando.length);
        // MODO DE IMPRESS�O FONTE B
        comando = new byte[]{27,77,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // MODO DE IMPRESS�O FONTE A
        comando = new byte[]{27,77,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("Comando 27, 116, n\n".getBytes(), "Comando 27, 116, n\n".length());
        // CODEPAGE 0
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 0\n".getBytes(), "CodePage 0\n".length());
        comando = new byte[]{27,116,0};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 1
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 1\n".getBytes(), "CodePage 1\n".length());
        comando = new byte[]{27,116,1};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 2
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 2\n".getBytes(), "CodePage 2\n".length());
        comando = new byte[]{27,116,2};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 3
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 3\n".getBytes(), "CodePage 3\n".length());
        comando = new byte[]{27,116,3};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 4
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 4\n".getBytes(), "CodePage 4\n".length());
        comando = new byte[]{27,116,4};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 5
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 5\n".getBytes(), "CodePage 5\n".length());
        comando = new byte[]{27,116,5};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 7
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 7\n".getBytes(), "CodePage 7\n".length());
        comando = new byte[]{27,116,7};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 8
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 8\n".getBytes(), "CodePage 8\n".length());
        comando = new byte[]{27,116,8};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 10
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 10\n".getBytes(), "CodePage 10\n".length());
        comando = new byte[]{27,116,10};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 13
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 13\n".getBytes(), "CodePage 13\n".length());
        comando = new byte[]{27,116,13};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 14
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 14\n".getBytes(), "CodePage 14\n".length());
        comando = new byte[]{27,116,14};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 15
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 15\n".getBytes(), "CodePage 15\n".length());
        comando = new byte[]{27,116,15};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 16
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 16\n".getBytes(), "CodePage 16\n".length());
        comando = new byte[]{27,116,16};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 17
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 17\n".getBytes(), "CodePage 17\n".length());
        comando = new byte[]{27,116,17};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 18
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 18\n".getBytes(), "CodePage 18\n".length());
        comando = new byte[]{27,116,18};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 19
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 19\n".getBytes(), "CodePage 19\n".length());
        comando = new byte[]{27,116,19};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 20
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 20\n".getBytes(), "CodePage 20\n".length());
        comando = new byte[]{27,116,20};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 21
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 21\n".getBytes(), "CodePage 21\n".length());
        comando = new byte[]{27,116,21};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 25
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 25\n".getBytes(), "CodePage 25\n".length());
        comando = new byte[]{27,116,25};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 26
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 26\n".getBytes(), "CodePage 26\n".length());
        comando = new byte[]{27,116,26};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 27
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 27\n".getBytes(), "CodePage 27\n".length());
        comando = new byte[]{27,116,27};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 32
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 32\n".getBytes(), "CodePage 32\n".length());
        comando = new byte[]{27,116,32};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 33
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 33\n".getBytes(), "CodePage 33\n".length());
        comando = new byte[]{27,116,33};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 34
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 34\n".getBytes(), "CodePage 34\n".length());
        comando = new byte[]{27,116,34};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 36
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 36\n".getBytes(), "CodePage 36\n".length());
        comando = new byte[]{27,116,36};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 37
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 37\n".getBytes(), "CodePage 37\n".length());
        comando = new byte[]{27,116,37};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 39
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 39\n".getBytes(), "CodePage 39\n".length());
        comando = new byte[]{27,116,39};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 40
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 40\n".getBytes(), "CodePage 40\n".length());
        comando = new byte[]{27,116,40};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 45
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 45\n".getBytes(), "CodePage 45\n".length());
        comando = new byte[]{27,116,45};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 46
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 46\n".getBytes(), "CodePage 460\n".length());
        comando = new byte[]{27,116,46};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 47
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 47\n".getBytes(), "CodePage 47\n".length());
        comando = new byte[]{27,116,47};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 48
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 48\n".getBytes(), "CodePage 48\n".length());
        comando = new byte[]{27,116,48};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 49
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 49\n".getBytes(), "CodePage 49\n".length());
        comando = new byte[]{27,116,49};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 50
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 50\n".getBytes(), "CodePage 50\n".length());
        comando = new byte[]{27,116,50};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 51
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 51\n".getBytes(), "CodePage 51\n".length());
        comando = new byte[]{27,116,51};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 52
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 52\n".getBytes(), "CodePage 52\n".length());
        comando = new byte[]{27,116,52};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 59
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 59\n".getBytes(), "CodePage 59\n".length());
        comando = new byte[]{27,116,59};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // CODEPAGE 68
        portaCom.writeBytes("Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length());
        portaCom.writeBytes("CodePage 68\n".getBytes(), "CodePage 68\n".length());
        comando = new byte[]{27,116,68};
        portaCom.writeBytes(comando, comando.length);
        portaCom.writeBytes("������������������\n\n".getBytes(), "������������������\n\n".length());
        // PULAR LINHA
        comando = new byte[]{0xA};
        portaCom.writeBytes(comando,comando.length);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        portaCom.writeBytes(comando,comando.length);
        
	}
}
