package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class DriverDll {
		
	int f;
	byte[] retorno;
	
	
	private FuncoesDll dll;
	
	private PointerByReference p = new PointerByReference();
	private Pointer p1;
	
	public DriverDll() {

		try {
			dll = (FuncoesDll) Native.loadLibrary("HprtPrinter.dll", FuncoesDll.class);
			System.out.println("Carregado");
		} catch (UnsatisfiedLinkError e) {
			System.out.println("Erro");
			e.printStackTrace();
		}
	}

	public void criarImpressora() {
		f = dll.PrtPrinterCreatorW(p, "i9".toCharArray());
		p1 = p.getValue();
	}
	public void abrirPorta() {
		f = dll.PrtPortOpenW(p1, "USB".toCharArray());
		p1 = p.getValue();
	}
	public void destruirimpressora() {
		f = dll.PrtPrinterDestroy(p1);
		p1 = p.getValue();
		System.out.println(f);
	}
	public void fecharPorta() {
		f = dll.PrtPortClose(p1);
		p1 = p.getValue();
		System.out.println(f);
	}
	public int menuPrincipalTunel() {
		
		System.out.println("**************************************************\n");
        System.out.println("*                                                *\n");
        System.out.println("*           EXEMPLO I9 - COMANDO DIRETO          *\n");
        System.out.println("*                                                *\n");
        System.out.println("**************************************************\n\n\n");
        System.out.println("�   [1]    IMPRESS�O CUPOM PADR�O - ELGIN.\n");
        System.out.println("�   [2]    ABRIR GAVETA.\n");
        System.out.println("�   [3]    STATUS DE IMPRESSORA, PAPEL E GAVETA.\n");
        System.out.println("�   [4]    IMPRESS�O CUPOM LAYOUT NOVO - ELGIN.\n");
        System.out.println("�   [5]    IMPRESS�O DE TODOS OS CODE128\n");
        System.out.println("�   [6]    IMPRESS�O DE TODAS AS FONTES E TAMANHOS.\n");
        System.out.println("�   [7]    FINALIZAR.\n");
        
        Scanner teclado = new Scanner(System.in);
        int t = teclado.nextInt();
        
        switch(t) {
        case 1:
        	cupomPadraoTunel();
        	break;
        case 2:
        	abrirGavetaTunel();
        	break;
        case 3:
        	break;
        case 4:
        	cupomNovoTunel();
        	break;
        case 5:
        	codigosDeBarraTunel();
        	break;
        case 6:
        	testeFontesTunel();
        	break;
        case 7:
        	destruirimpressora();
        	fecharPorta();
        	System.out.println("Porta Fechada com Sucesso.");
        	break;
        }
        
        return t;
	}
	public void cupomPadraoTunel() {
		// DATA E HORA ATUAL
        Date agora = new Date();
        
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String data2 = format.format(agora);
		
		 // LIMPANDO O BUFFER DE IMPRESS�O.
        byte[] comando = new byte[]{0x1B,0x40};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "Elgin Manaus\n".getBytes(), "Elgin Manaus\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Elgin/SA\n".getBytes(), "Elgin/SA\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Rua Abiurana, 579 Distrito Industrial Manaus - AM\n\n".getBytes(), "Rua Abiurana, 579 Distrito Industrial Manaus - AM\n\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM\n".getBytes(), "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        // CANCELANDO FONTE ANTERIOR
        comando = new byte[]{0x1B,0x21,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MUDANDO FONTE.
        comando = new byte[]{0x1B,0x21,0x08};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESSOS.
        f = dll.PrtDirectIO(p1, "EXTRATO No. 002046\n".getBytes(), "EXTRATO No. 002046\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CUPOM FISCAL ELETRONICO - SAT\n".getBytes(), "CUPOM FISCAL ELETRONICO - SAT\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);;
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);;
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "CPF/CNPJ consumidor: \n".getBytes(), "CPF/CNPJ consumidor: \n".length(), retorno, 0, 0);
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM r$\n".getBytes(), "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM r$\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "| 1000016 VRAAAAAAAAAAAAAAAAAAAAAAAU 3,449 L 2,9++ (3,84) 10,00\n\n".getBytes(), "| 1000016 VRAAAAAAAAAAAAAAAAAAAAAAAU 3,449 L 2,9++ (3,84) 10,00\n\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Subtotal                                                  10,00\n".getBytes(), "Subtotal                                                  10,00\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "TOTAL R$                                                  10,00\n\n".getBytes(), "TOTAL R$                                                  10,00\n\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Dinheiro                                                  10,00\n".getBytes(), "Dinheiro                                                  10,00\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Troco R$                                                   0,00\n\n".getBytes(), "Troco R$                                                   0,00\n\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Tributos Totais (Lei Fed 12.741/12) R$                      3,85\n".getBytes(), "Tributos Totais (Lei Fed 12.741/12) R$                      3,85\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est\n".getBytes(), "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Fonte: IBPT/FECOMECIO (RS) 9oi3aC\n\n".getBytes(), "Fonte: IBPT/FECOMECIO (RS) 9oi3aC\n\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "KM: 0\n".getBytes(), "KM: 0\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // TEXTO A ESQUERDA.
        comando = new byte[]{0x1B,0x61,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "* Valor aproximado dos tributos do item\n".getBytes(), "* Valor aproximado dos tributos do item\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZAR TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, "----------------------------------------------------------------\n".getBytes(), "----------------------------------------------------------------\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CANCELANDO FONTE ANTERIOR0
        comando = new byte[]{0x1B,0x21,0x0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MUDANDO FONTE.
        comando = new byte[]{0x1B,0x21,0x08};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESSOS.
        f = dll.PrtDirectIO(p1, "SAT No. 900001231\n".getBytes(), "SAT No. 900001231\n".length(), retorno, 0, 0);
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDAR IMPRESSOS.
        f = dll.PrtDirectIO(p1, data2.getBytes(), data2.length(), retorno, 0, 0);
        comando = new byte[]{0xA,0xA};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        
        /********************************************************** 
         **********************************************************
         *********IMPRESS�O DO CODE 128 - C�DIGO DE BARRAS*********
         **********************************************************
         **********************************************************/
        
        
        // LIMPANDO O BUFFER DE IMPRESS�O.
        comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{0x1B,0x61,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO ALTURA DO C�DIGO DE BARRAS.
        comando = new byte[]{0x1D,0x68,70};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO LARGURA DO C�DIGO DE BARRAS.
        comando = new byte[]{0x1D,0x77,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO POSI��O DOS CARACTERES EM HRI.
        comando = new byte[]{0x1D,0x48,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        String code128 = "{B35150661099008000141593515066109900800014159";
        int tamanho = code128.length();
        
        // MANDAR IMPRESSOS.
        comando = new byte[]{0x1D,0x6B,0x49,(byte)tamanho};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, code128.getBytes(), code128.length(), retorno, 0, 0);
        comando = new byte[]{0xA,0xA};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        
        /**********************************************************\ 
         **********************************************************
         *****************IMPRESS�O DO QR CODE*********************
         **********************************************************
        \**********************************************************/
        
        
        int pl;
        int ph;
        String qrcode = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmoxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrubensborgesdeandrade";
        int taman = qrcode.length()+3;
        
        if(taman>=255) {
            pl = taman%256;
            ph = taman/256;
            
        } else {
            
            pl = taman;
            ph = 0;
        }
        
        // CENTRALIZANDO O TEXTO.
        comando = new byte[]{27, 97, 49};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO O TAMANHO DO QRCODE
        comando = new byte[]{29, 40, 107, 3, 0, 49, 67, 5};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFINIR N�VEL DE CORRE��O
        comando = new byte[]{29, 40, 107, 3, 0, 49, 69, 48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // ARMAZENAMENTO DE DADOS.
        comando = new byte[]{0x1D, 0x28, 0x6B, (byte)pl, (byte)ph, 0x31, 0x50, 0x30};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        comando = qrcode.getBytes();
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        comando = new byte[]{29, 40, 107, 3, 0, 49, 81, 48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // PULAR LINHA
        comando = new byte[]{0xA};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	}
	public void abrirGavetaTunel() {
		// ABRIR GAVETA
        byte[] comando = new byte[]{27, 112, 0, 100, 100}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	}
	public void statusTunel() {
		
	}
	public void cupomNovoTunel() {
		
		// LIMPANDO O BUFFER
        byte[]comando = new byte[]{0x18};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, 24, 00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, 0x10, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
		// IMAGEM
		comando = new byte[]{0x1C, 0x70, 0x01, 0x00}; 
		f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
		// SELE��O DO MODO P�GINA.
        comando = new byte[]{0x1B, 0x4C}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        
        // DEFININDO �REA DE IMPRESS�O.
        comando = new byte[]{0x1B, 0x57, 0x00, 0x00, 0x00, 0x00, 0x48, 0x02, 24, 06}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        // SELE��O DA DIRE��O DE IMPRESS�O NO MODO P�GINA.
        comando = new byte[]{0x1B, 0x54, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, 0x10, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);;
        
        f = dll.PrtDirectIO(p1, "CNPJ 00.000.000/000-99 Razao Social da Empresa\n".getBytes(), "CNPJ 00.000.000/000-99 Razao Social da Empresa\n".length(), retorno, 0, 0);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Av da Tecnologia, 030, Centro, Rio de Janeiro, RJ\n".getBytes(), "Av da Tecnologia, 030, Centro, Rio de Janeiro, RJ\n".length(), retorno, 0, 0);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Doc. Aux. da Nota Fiscal de Consumidor Eletronica\n".getBytes(), "Doc. Aux. da Nota Fiscal de Consumidor Eletronica\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Codigo    Descricao        Qtde UN      Vl Unit    Vl Total\n".getBytes(), "Codigo    Descricao        Qtde UN      Vl Unit    Vl Total\n".length(), retorno, 0, 0);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "003277    Impressora Elgin I9   1 Peca   100.00      100.00\n".getBytes(), "003277    Impressora Elgin I9   1 Peca   100.00      100.00\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "085273    Impressora Elgin I7   1 Peca   100.00      100.00\n".getBytes(), "085273    Impressora Elgin I7   1 Peca   100.00      100.00\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Qtde. total de itens                                      2\n".getBytes(), "Qtde. total de itens                                      2\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Valor Total R$                                       200,00\n".getBytes(), "Valor Total R$                                       200,00\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Valor a Pagar R$                                     150,00\n\n".getBytes(), "Valor a Pagar R$                                     150,00\n\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Valor a Pagar R$                                     150,00\n\n".getBytes(), "Valor a Pagar R$                                     150,00\n\n".length(), retorno, 0, 0);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "FORMA PAGAMENTO                               VALOR PAGO R$\n".getBytes(), "FORMA PAGAMENTO                               VALOR PAGO R$\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Cartao de Credito                                75,00\n".getBytes(), "Cartao de Credito                                75,00\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Cartao de Credito                                75,00\n\n".getBytes(), "Cartao de Credito                                75,00\n\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)0x85, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Consulte pela chave de acesso em\n".getBytes(), "Consulte pela chave de acesso em\n".length(), retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)0x95, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "http://nfce.fazenda.rj.gov.br/consulta \n".getBytes(), "http://nfce.fazenda.rj.gov.br/consulta \n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x40, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000\n".getBytes(), "0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000\n".length(), retorno, 0, 0);
        
        // QR CODE
        
        int pl2;
        int ph2;
        String qrcode2 = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmoxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrubensborgesdeandrade";
        int taman2 = qrcode2.length()+3;
        
        if(taman2>=255) {
            pl2 = taman2%256;
            ph2 = taman2/256;
            
        } else {
            
            pl2 = taman2;
            ph2 = 0;
        }
        
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, 0x28, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO O TAMANHO DO QRCODE
        comando = new byte[]{29, 40, 107, 3, 0, 49, 67, 2};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFINIR N�VEL DE CORRE��O
        comando = new byte[]{29, 40, 107, 3, 0, 49, 69, 48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // ARMAZENAMENTO DE DADOS.
        comando = new byte[]{0x1D, 0x28, 0x6B, (byte)pl2, (byte)ph2, 0x31, 0x50, 0x30};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        comando = qrcode2.getBytes();
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        comando = new byte[]{29, 40, 107, 3, 0, 49, 81, 48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
        // IMPRESS�O DOS DEMAIS DADOS.
        
        // DEFINI��O DA POSI��O DE IMPRESS�O NA HORIZONTAL.
        comando = new byte[]{0x1B, 0x24, (byte)0x120, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // DEFINI��O DA POSI��O DE IMPRESS�O NA VERTICAL.
        comando = new byte[]{0x1D, 0x24, (byte)0x186, 0x04};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CONSUMIDOR NAO IDENTIFICADO\n".getBytes(), "CONSUMIDOR NAO IDENTIFICADO\n".length(), retorno, 0, 0);
        // DEFININDO TIPO E TAMANHO DA FONTE.
        comando = new byte[]{0x1B,0x21,0x01};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CANCELANDO NEGRITO
        comando = new byte[]{0x1B, 0x45, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "NFC-E N0000001 Serie 001 04/12/2017 11:40:51".getBytes(), "NFC-E N0000001 Serie 001 04/12/2017 11:40:51".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Protocolo de autoriza��o: 314 1300004001 80\n".getBytes(), "Protocolo de autoriza��o: 314 1300004001 80\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Data de Autorizacao 04/12/2017 11:40:51\n".getBytes(), "Data de Autorizacao 04/12/2017 11:40:51\n".length(), retorno, 0, 0);
        // POSI��O DA IMPRESS�O
        comando = new byte[]{0x1B, 0x24, (byte)180, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Trib Totais Incidentes(Lei Fed. 12.741/2012)\n".getBytes(), "Trib Totais Incidentes(Lei Fed. 12.741/2012)\n".length(), retorno, 0, 0);
        // IMPRESS�O EM MODO P�GINA
        comando = new byte[]{0x0C};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        
	}
	public void codigosDeBarraTunel() {
		
		// CODE128
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       byte[]comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       f = dll.PrtDirectIO(p1, "Code128\n".getBytes(), "Code128\n".length(), retorno, 0, 0);
	       	       
	       String acode128 = "{B1234567891011121";
	       int atamanho = acode128.length();
	       
	       comando = new byte[]{0x1D,0x6B,0x49,(byte)atamanho};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       f = dll.PrtDirectIO(p1, acode128.getBytes(), acode128.length(), retorno, 0, 0);
	       comando = new byte[]{0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE UPC-A
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       f = dll.PrtDirectIO(p1, "Code UPCA\n".getBytes(), "Code UPCA\n".length(), retorno, 0, 0);
	       	       
	       String upcacode = "01234567012";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 0};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = upcacode.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE UPCE
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Code UPCE\n".getBytes(), "Code UPCE\n".length(), retorno, 0, 0);
	       	       
	       String upcecode = "01234567890";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = upcecode.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE EAN8
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Code EAN8\n".getBytes(), "Code EAN8\n".length(), retorno, 0, 0);
	       
	       String ean8code = "01234567";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 3};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = ean8code.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE EAN13
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Code EAN13\n".getBytes(), "Code EAN13\n".length(), retorno, 0, 0);
	       	       
	       String ean13code = "0123456789100";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = ean13code.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE39
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Code39\n".getBytes(), "Code39\n".length(), retorno, 0, 0);
	       	       
	       String code39 = "*0123456789*";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 4};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = code39.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE ITF
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "CodeITF\n".getBytes(), "CodeITF\n".length(), retorno, 0, 0);
	       
	       
	       String codeitf = "0123456789";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 5};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = codeitf.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE CODABAR
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Codabar\n".getBytes(), "Codabar\n".length(), retorno, 0, 0);
	       	       
	       String codabar = "A40156B";
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 6};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = codabar.getBytes();
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0,0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       // CODE CODE93
	       
	       // LIMPANDO O BUFFER DE IMPRESS�O.
	       comando = new byte[]{0x1B,0x40}; 
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // CENTRALIZANDO O TEXTO.
	       comando = new byte[]{0x1B,0x61,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO TIPO E TAMANHO DA FONTE.
	       comando = new byte[]{0x1B,0x21,0x1};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO ALTURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x68,70};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO LARGURA DO C�DIGO DE BARRAS.
	       comando = new byte[]{0x1D,0x77,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       // DEFININDO POSI��O DOS CARACTERES EM HRI.
	       comando = new byte[]{0x1D,0x48,2};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       
	       f = dll.PrtDirectIO(p1, "Code93\n".getBytes(), "Code93\n".length(), retorno, 0, 0);
	       
	       String code93 = "*TESTECODE93*";
	       int code93tamanho = code93.length();
	       
	       // MANDAR IMPRESSOS.
	       comando = new byte[]{29, 107, 72,(byte)code93tamanho};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       f = dll.PrtDirectIO(p1, code93.getBytes(), code93.length(), retorno, 0, 0);
	       comando = new byte[]{0xA,0xA};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	       comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
	       f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	}
	public void testeFontesTunel() {
		// LIMPANDO O BUFFER DE IMPRESS�O.
        byte[]comando = new byte[]{0x1B,0x40}; 
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 1\n".getBytes(), "Altura 1\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 2\n".getBytes(), "Altura 2\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,2};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 3\n".getBytes(), "Altura 3\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,3};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 4\n".getBytes(), "Altura 4\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,4};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 5\n".getBytes(), "Altura 5\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,5};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 6\n".getBytes(), "Altura 6\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,6};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 7\n".getBytes(), "Altura 7\n".length(), retorno, 0, 0);
        // ALTURA
        comando = new byte[]{29,33,7};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Altura 8\n".getBytes(), "Altura 8\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 1\n".getBytes(), "Largura 1\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,16};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 2\n".getBytes(), "Largura 2\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,32};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 3\n".getBytes(), "Largura 3\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 4\n".getBytes(), "Largura 4\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,64};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 5\n".getBytes(), "Largura 5\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,80};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 6\n".getBytes(), "Largura 6\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,96};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 7\n".getBytes(), "Largura 7\n".length(), retorno, 0, 0);
        // LARGURA
        comando = new byte[]{29,33,112};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MANDANDO IMPRESS�O
        f = dll.PrtDirectIO(p1, "Largura 8\n".getBytes(), "Largura 8\n".length(), retorno, 0, 0);
        // LARGURA/ALTURA
        comando = new byte[]{29,33,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{27,69,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Com Negrito\n".getBytes(), "Com Negrito\n".length(), retorno, 0, 0);
        // SEM NEGRITO
        comando = new byte[]{27,69,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Sem Negrito\n".getBytes(), "Sem Negrito\n".length(), retorno, 0, 0);
        // MODO DE IMPRESS�O FONTE B
        comando = new byte[]{27,77,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "MiniFont\n".getBytes(), "MiniFont\n".length(), retorno, 0, 0);
        // MODO DE IMPRESS�O FONTE A
        comando = new byte[]{27,77,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Fonte Normal\n".getBytes(), "Fonte Normal\n".length(), retorno, 0, 0);
        // MODO DE IMPRESS�O FONTE C
        comando = new byte[]{27,77,2};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Fonte C\n\n".getBytes(), "Fonte C\n\n".length(), retorno, 0, 0);
        // MODO SUBLINHADO
        comando = new byte[]{27,33,(byte)128};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Sublinhado\n".getBytes(), "Sublinhado\n".length(), retorno, 0, 0);
        // SEM SUBLINHADO
        comando = new byte[]{27,33,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Sem Sublinhado\n\n".getBytes(), "Sem Sublinhado\n\n".length(), retorno, 0, 0);
        // ALINHADO A ESQUERDA
        comando = new byte[]{27,97,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Alinhado a Esquerda\n".getBytes(), "Alinhado a Esquerda\n".length(), retorno, 0, 0);
        // ALINHAMENTO CENTRAL
        comando = new byte[]{27,97,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Centralizado\n".getBytes(), "Centralizado\n".length(), retorno, 0, 0);
        // ALINHADO A DIREITA
        comando = new byte[]{27,97,2};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Alinhado a Direita\n\n".getBytes(), "Alinhado a Direita\n\n".length(), retorno, 0, 0);
        // ALINHADO A ESQUERDA
        comando = new byte[]{27,97,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        

        // CARACTERES ESPECIAIS            
        comando = new byte[]{0x1B, 0x40};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // TABELA DE C�DIGOS DE CARACTERES
        comando = new byte[]{27,116,16};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n".getBytes(), "������������������\n".length(), retorno, 0, 0);
        // MODO SUBLINHADO
        comando = new byte[]{27,33,(byte)128};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n".getBytes(), "������������������\n".length(), retorno, 0, 0);
        // SEM SUBLINHADO
        comando = new byte[]{27,33,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // NEGRITO
        comando = new byte[]{27,69,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n".getBytes(), "������������������\n".length(), retorno, 0, 0);
    	// SEM NEGRITO
        comando = new byte[]{27,69,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // MODO DE IMPRESS�O FONTE B
        comando = new byte[]{27,77,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // MODO DE IMPRESS�O FONTE A
        comando = new byte[]{27,77,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "Comando 27, 116, \n".getBytes(), "Comando 27, 116, \n".length(), retorno, 0, 0);
        // CODEPAGE 0
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 0\n".getBytes(), "CodePage 0\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,0};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 1
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 1\n".getBytes(), "CodePage 1\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,1};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 2
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 2\n".getBytes(), "CodePage 2\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,2};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 3
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 3\n".getBytes(), "CodePage 3\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,3};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 4
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 4\n".getBytes(), "CodePage 4\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,4};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 5
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 5\n".getBytes(), "CodePage 5\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,5};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 7
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 7\n".getBytes(), "CodePage 7\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,7};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 8
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 8\n".getBytes(), "CodePage 8\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,8};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 10
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 10\n".getBytes(), "CodePage 10\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,10};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 13
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 13\n".getBytes(), "CodePage 13\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,13};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 14
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 14\n".getBytes(), "CodePage 14\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,14};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 15
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 15\n".getBytes(), "CodePage 15\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,15};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 16
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 16\n".getBytes(), "CodePage 16\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,16};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 17
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 17\n".getBytes(), "CodePage 17\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,17};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 18
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 18\n".getBytes(), "CodePage 18\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,18};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 19
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 19\n".getBytes(), "CodePage 19\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,19};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 20
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 20\n".getBytes(), "CodePage 20\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,20};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 21
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 21\n".getBytes(), "CodePage 21\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,21};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 25
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 25\n".getBytes(), "CodePage 25\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,25};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 26
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 26\n".getBytes(), "CodePage 26\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,26};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 27
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 27\n".getBytes(), "CodePage 27\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,27};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 32
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 32\n".getBytes(), "CodePage 32\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,32};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 33
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 33\n".getBytes(), "CodePage 33\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,33};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 34
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 34\n".getBytes(), "CodePage 34\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,34};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 36
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 36\n".getBytes(), "CodePage 36\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,36};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 37
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 37\n".getBytes(), "CodePage 37\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,37};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 39
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 39\n".getBytes(), "CodePage 39\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,39};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 40
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 40\n".getBytes(), "CodePage 40\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,40};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 45
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 45\n".getBytes(), "CodePage 45\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,45};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 46
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 46\n".getBytes(), "CodePage 46\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,46};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 47
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 47\n".getBytes(), "CodePage 47\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,47};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 48
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 48\n".getBytes(), "CodePage 48\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,48};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 49
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 49\n".getBytes(), "CodePage 49\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,49};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 50
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 50\n".getBytes(), "CodePage 50\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,50};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 51
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 51\n".getBytes(), "CodePage 51\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,51};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 52
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 52\n".getBytes(), "CodePage 52\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,52};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 59
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 59\n".getBytes(), "CodePage 59\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,59};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // CODEPAGE 68
        f = dll.PrtDirectIO(p1, "Teste Caracteres Especiais\n".getBytes(), "Teste Caracteres Especiais\n".length(), retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "CodePage 68\n".getBytes(), "CodePage 68\n".length(), retorno, 0, 0);
        comando = new byte[]{27,116,68};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        f = dll.PrtDirectIO(p1, "������������������\n\n".getBytes(), "������������������\n\n".length(), retorno, 0, 0);
        // PULAR LINHA
        comando = new byte[]{0xA};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
        // CORTAR PAPEL
        comando = new byte[]{0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00};
        f = dll.PrtDirectIO(p1, comando, comando.length, retorno, 0, 0);
	}
}
