package controller;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public interface FuncoesDll extends Library { 
	
	public int PrtPrinterCreatorW(PointerByReference p, char[] model);
	public int PrtPrinterDestroy(Pointer p);
	public int PrtPortOpenW(Pointer p, char[] ioSettings );
	public int PrtPortClose(Pointer p);
	public int PrtDirectIO(Pointer p, byte[] writeData, int writeNum, byte[] readData, int readNum, int preadedNum);
	
}
